/*
Copyright © 2020 Noukkis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package anilist

import (
	"context"
	"fmt"
	"strings"

	"github.com/shurcooL/graphql"
)

var endpoint = "https://graphql.anilist.co"
var client = graphql.NewClient(endpoint, nil)

type date struct {
	Year  graphql.Int
	Month graphql.Int
	Day   graphql.Int
}

type media struct {
	Id           graphql.Int
	Format       graphql.String
	StartDate    date
	EndDate      date
	Episodes     graphql.Int
	Duration     graphql.Int
	AverageScore graphql.Int
	Popularity   graphql.Int
	Genres       []graphql.String
	Title        struct {
		UserPreferred graphql.String
		Native        graphql.String
		English       graphql.String
		Romaji        graphql.String
	}
}

type entry struct {
	Status      graphql.String
	Score       graphql.Int
	Repeat      graphql.Int
	StartedAt   date
	CompletedAt date
	Media       media
}

type query struct {
	MediaListCollection struct {
		Lists []struct {
			IsCustomList graphql.Boolean
			Entries      []entry
		}
	} `graphql:"MediaListCollection(userName:$user type:ANIME)"`
}

// List retrieves an user's list of animes
func List(user string) []map[string]string {
	var q query = request(user)
	var res []map[string]string
	for _, l := range q.MediaListCollection.Lists {
		if !l.IsCustomList {
			for _, e := range l.Entries {
				res = append(res, parseMedia(e))
			}
		}
	}

	return res
}

func addDate(d date, entry map[string]string, key string) {
	entry[key+"Year"] = fmt.Sprintf("%04d", d.Year)
	entry[key+"Month"] = fmt.Sprintf("%02d", d.Month)
	entry[key+"Day"] = fmt.Sprintf("%02d", d.Day)
	entry[key] = fmt.Sprintf("%04d-%02d-%02d", d.Year, d.Month, d.Day)
}

func itoa(i graphql.Int) string {
	return fmt.Sprintf("%d", i)
}

func atoa(arr []graphql.String) string {
	var strArr []string
	for _, s := range arr {
		strArr = append(strArr, string(s))
	}
	return "[" + strings.Join(strArr, ",") + "]"
}

func parseMedia(e entry) map[string]string {
	res := make(map[string]string)
	res["status"] = string(e.Status)
	res["score"] = itoa(e.Score)
	res["repeat"] = itoa(e.Repeat)
	res["id"] = itoa(e.Media.Id)
	res["format"] = string(e.Media.Format)
	res["episodes"] = itoa(e.Media.Episodes)
	res["duration"] = itoa(e.Media.Duration)
	res["average"] = itoa(e.Media.AverageScore)
	res["popularity"] = itoa(e.Media.Popularity)
	res["title"] = string(e.Media.Title.UserPreferred)
	res["english"] = string(e.Media.Title.English)
	res["romaji"] = string(e.Media.Title.Romaji)
	res["native"] = string(e.Media.Title.Native)
	res["genres"] = atoa(e.Media.Genres)
	addDate(e.StartedAt, res, "started")
	addDate(e.CompletedAt, res, "completed")
	addDate(e.Media.StartDate, res, "start")
	addDate(e.Media.EndDate, res, "end")
	return res
}

func request(user string) query {
	var q query
	v := map[string]interface{}{
		"user": graphql.String(user),
	}
	err := client.Query(context.Background(), &q, v)
	if err != nil {
		panic(err)
	}
	return q
}
