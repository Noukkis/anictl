/*
Copyright © 2020 Noukkis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package fs

import (
	"anictl/model"
	"encoding/gob"
	"os"
	"path"

	"github.com/mitchellh/go-homedir"
)

// DefaultConfigPath retrieve the default config path of the machine
func DefaultConfigPath() string {
	home, err := homedir.Dir()
	if err != nil {
		panic(err)
	}
	config := path.Join(home, ".config/anictl")
	err = os.MkdirAll(config, os.ModePerm)
	if err != nil {
		panic(err)
	}
	return config
}

// SaveLib serializes the library to a file
func SaveLib(lib model.Lib, savePath string) {
	file, _ := os.Create(savePath)
	defer file.Close()

	e := gob.NewEncoder(file)

	// Encoding the map
	err := e.Encode(lib)
	if err != nil {
		panic(err)
	}
}

// RetrieveLib retrieves the library from a serialized file
func RetrieveLib(savePath string) model.Lib {
	file, err := os.Open(savePath)
	if err != nil {
		return make(model.Lib)
	}

	d := gob.NewDecoder(file)
	var res model.Lib

	// Decoding the map
	err = d.Decode(&res)
	if err != nil {
		panic(err)
	}

	return res
}
