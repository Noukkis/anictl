/*
Copyright © 2020 Noukkis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package lib

import (
	"anictl/app/anilist"
	"anictl/app/fs"
	"anictl/model"
	"strings"
	"time"
)

// List lists the user's animes, taking them from cache or from anilist.co
func List(user string, maxAge int64, libPath string) []map[string]string {
	user = strings.ToLower(user)
	var lib model.Lib = fs.RetrieveLib(libPath)
	now := time.Now().UTC().Unix()
	entry, hasEntry := lib[user]
	if !hasEntry || maxAge < 0 || now-entry.Timestamp > maxAge {
		list := anilist.List(user)
		lib[user] = model.LibEntry{Timestamp: now, List: list}
		fs.SaveLib(lib, libPath)
	}
	return lib[user].List
}
