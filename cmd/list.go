/*
Copyright © 2020 Noukkis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"anictl/app/lib"
	"fmt"
	"math"
	"regexp"
	"strings"

	"github.com/spf13/viper"

	"github.com/spf13/cobra"
)

var varsRegex = regexp.MustCompile(`\$\w+`)
var regexFilterRegex = regexp.MustCompile(`^(\w+)::(.+)$`)
var classicFilterRegex = regexp.MustCompile(`^(\w+):(.+)$`)

var force bool
var lazy bool
var all bool

type pair struct {
	key   string
	value string
}

var listCmd = &cobra.Command{
	Use:    "list",
	Short:  "List the animes in an user's anilist",
	PreRun: initList,
	Run:    runList,
}

func init() {
	rootCmd.AddCommand(listCmd)
	listCmd.Flags().StringP("user", "u", "", "the user from whom to get the list")
	listCmd.Flags().StringP("format", "f", "", "the output format")
	listCmd.Flags().BoolVarP(&force, "force", "F", false, "will force synchronizing the DB")
	listCmd.Flags().BoolVarP(&lazy, "lazy", "L", false, "will try to avoid synchronizing the DB")
	listCmd.Flags().BoolVarP(&all, "all", "a", false, "will print all infos about the each entry, ignoring the '-f' flag")
}

func initList(cmd *cobra.Command, args []string) {
	_ = viper.BindPFlag("user", cmd.Flags().Lookup("user"))
	_ = viper.BindPFlag("format", cmd.Flags().Lookup("format"))
}

func runList(cmd *cobra.Command, args []string) {
	user := viper.GetString("user")
	format := viper.GetString("format")
	maxAge := viper.GetInt64("maxAge")
	libPath := viper.GetString("lib")
	regexFilters, classicFilters, titleFilters := getFilters(args)

	if force {
		maxAge = -1
	} else if lazy {
		maxAge = math.MaxInt64
	}

	list := lib.List(user, maxAge, libPath)

	for _, media := range list {
		if filter(media, regexFilters, classicFilters, titleFilters) {
			if all {
				fmt.Println(media["title"])
				for key, value := range media {
					fmt.Printf("\t%s: %s\n", key, value)
				}
			} else {
				fmt.Println(formatOutput(format, media))
			}
		}
	}
}

func filter(media map[string]string, regexFilters []pair, classicFilters []pair, titleFilters []string) bool {
	for _, f := range classicFilters {
		if !strContains(media[f.key], f.value) {
			return false
		}
	}
	for _, f := range regexFilters {
		b, err := regexp.MatchString(f.value, media[f.key])
		if err != nil {
			panic(err)
		}
		if !b {
			return false
		}
	}
	for _, f := range titleFilters {
		if !(strContains(media["title"], f) ||
			strContains(media["english"], f) ||
			strContains(media["romaji"], f) ||
			strContains(media["native"], f)) {
			return false
		}
	}
	return true
}

func getFilters(args []string) ([]pair, []pair, []string) {
	var regex []pair
	var classic []pair
	var title []string
	for _, arg := range args {
		if regexFilterRegex.MatchString(arg) {
			split := regexFilterRegex.FindAllStringSubmatch(arg, -1)[0]
			regex = append(regex, pair{split[1], split[2]})
		} else if classicFilterRegex.MatchString(arg) {
			split := classicFilterRegex.FindAllStringSubmatch(arg, -1)[0]
			classic = append(classic, pair{split[1], split[2]})
		} else {
			title = append(title, arg)
		}
	}
	return regex, classic, title
}

func formatOutput(format string, media map[string]string) string {
	for _, v := range getVariables(format) {
		if val, ok := media[v]; ok {
			format = strings.ReplaceAll(format, "$"+v, val)
		}
	}
	return format
}

func getVariables(format string) []string {
	vars := varsRegex.FindAllString(format, -1)
	for i, v := range vars {
		vars[i] = v[1:]
	}
	return removeDuplicateValues(vars)
}

func removeDuplicateValues(intSlice []string) []string {
	keys := make(map[string]bool)
	var list []string
	for _, entry := range intSlice {
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}
	return list
}

func strContains(container string, value string) bool {
	return strings.Contains(strings.ToUpper(container), strings.ToUpper(value))
}
